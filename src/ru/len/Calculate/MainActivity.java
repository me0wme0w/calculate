package ru.len.Calculate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void OnClick(View view) {
        Intent intent = new Intent(this,Main2Activity.class);
        EditText Text1 = (EditText)findViewById(R.id.editText);
        intent.putExtra("PERV", Text1.getText().toString());

        EditText Text2 = (EditText)findViewById(R.id.editText2);
        intent.putExtra("SEC", Text2.getText().toString());
        startActivity(intent);
    }
}