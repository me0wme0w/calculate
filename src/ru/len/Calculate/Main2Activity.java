package ru.len.Calculate;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Елена on 04.02.2015.
 */
public class Main2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);

        String message;
        message=getIntent().getExtras().getString("PERV");
        TextView perv = (TextView)findViewById(R.id.perv_sl);
        perv.setText(message);

        String message2;
        message2=getIntent().getExtras().getString("SEC");
        TextView vt = (TextView)findViewById(R.id.vt_sl);

        if(Integer.parseInt(message2) < 0)
        {
            vt.setText("("+message2+")");
        }
        else
        {
            vt.setText(message2);
        }

        int m = Integer.parseInt(message);
        int m2 = Integer.parseInt(message2);
        int sum = m + m2;
        TextView summa = (TextView) findViewById(R.id.summa);
        summa.setText(Integer.toString(sum));
        }

    }